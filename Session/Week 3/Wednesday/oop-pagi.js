// INTRO OOP
// class Human {
//     //Properties
//     constructor(name,age,skills){
//         this.name = name;
//         this.age = age;
//         this.skills = skills || []
//     }

//     //Method
//     getAge(){
//         console.log(this.age);
//     }
//     getSkill(){
//         console.log(this.skills);
//     }
//     // addSkills(arraySkills){
//     //     this.skills.push(arraySkills);
//     // }
//     growUp(){
//         this.age += 1;
//     }
// }
// Human.prototype.addSkills = function(skill){
//     this.skills.push(skill);
// };

// let cindy = new Human("Cindy",18);
// console.log(cindy);
// cindy.addSkills("Javascript")
// cindy.addSkills("Python")
// console.log(cindy);
// cindy.growUp();
// cindy.growUp();
// cindy.growUp();
// console.log(cindy);

// class Programmer {
//     constructor(level){
//         this.level = level || 1;
//     }
//     static addLevel(){
//         this.level += 1;
//         // return "Static";
//     }
// }
// let bill = new Programmer();
// // console.log(bill.level)
// Programmer.addLevel();
// // console.log(bill.level)

// // console.log(Programmer.addLevel())

// BASIC CONCEPT
//1 & 2 - Inheritance & Polymorphism
// class Human {
//     constructor(name,age){
//         this.name = name;
//         this.age = age;
//     }
//     introduce(){
//         console.log(`Hi, i'm ${this.name} and ${this.age} years old.`)
//     }
//     work(){
//         console.log("I'm working now.")
//     }
// }
// let james = new Human("James",21);
// james.introduce();

// class Programmer extends Human {
//     constructor(name,age,skills){
//         super(name,age);
//         this.skills = skills || [];
//     }

//     introduce(){
//         super.introduce();
//         console.log("I'm Programmer");
//     }
//     code(){
//         console.log(`My skills are ${this.skills}`)
//     }
// }

// let karina = new Programmer("Karina",19,["Javascript","Python"]);
// // console.log(karina)
// // console.log(james)
// karina.introduce();
// james.introduce();
// karina.code();
// james.code();

// try {
//     james.code();
// } catch(err){
//     console.log(err)
// }

// 3 - Encapsulation
class Human {
    constructor(name,age){
        this._name = name;
        this._age = age;
    }
    //Getter
    get name(){
        return this._name;
    }
    get age(){
        return this._age;
    }
    // Setter
    set setName(name){
        this._name = name;
    }
    set setAge(age){
        this._age = age;
    }
    introduce(){
        console.log(`Hi, i'm ${this.name} and ${this.age} years old.`)
    }
    work(){
        console.log("I'm working now.")
    }
}
let andy = new Human("Andy",25);
console.log(andy.name);
andy.setName = "ANDI"
console.log(andy.name);
andy.introduce();


