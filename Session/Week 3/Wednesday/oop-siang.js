// INTRO OOP
// const person = {
//     name : "Jack",
//     age : 18
// }
// class Human {
//     // Properties
//     constructor(name,age,skills){
//         this.name = name;
//         this.age = age;
//         this.skills = skills || [];
//     }
//     // Method
//     introduce(){
//         console.log(`Hi, i'm ${this.name} and ${this.age} years old.`)
//     }
//     work(){
//         console.log(`Im Working now.`)
//     }
//     growUp(){
//         this.age += 1;
//     }
// }

// Human.prototype.addSkills = function(skill){
//     this.skills.push(skill);
// };

// let jack = new Human("Jack", 23);
// console.log(jack);
// jack.introduce();
// jack.growUp();
// jack.growUp();
// console.log(jack);
// jack.addSkills("Javascript");
// jack.addSkills("Python");
// console.log(jack);

// class Person {
//     static work(){
//         console.log("Class Person works.")
//     }
// }
// Person.work();


// 1 & 2 - Inheritance & Polymorphism
class Person {
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    introduce() {
        console.log(`Konnichiwa, i'm ${this.name} and level ${this.level}.`)
    }

    work() {
        console.log("I'm working now.")
    }
}
let orang = new Person("Jack", 23);
// orang.introduce();

class Pirate extends Person {
    constructor(name, level, power) {
        super(name, level);
        this.power = power;
    }

    introduce() {
        super.introduce();
        console.log("Ore wa kaizoku.")
    }

    sailing() {
        console.log("I'm sailing to Laugh Tale!")
    }

}

let luffy = new Pirate("Luffy", 50, "Conqueror Haki");
// luffy.introduce();
// luffy.sailing();
// person.sailing();

// 3 - Encapsulation

class Human {
    constructor(name,age){
        this._name = name;
        this._age = age;
    }
    get name(){
        return this._name;
    }
    set setName(name){
        this._name = name;
    }
    
}

let jack = new Human("Jack",23);
console.log(jack.name);
jack.setName = "Jake";
console.log(jack.name);
