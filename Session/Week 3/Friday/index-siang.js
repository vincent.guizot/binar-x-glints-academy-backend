//Process Argv
// const data = process.argv;
// const command = process.argv[2];
// const params = process.argv.slice(3);
// console.log(data);
// console.log(command);
// console.log(params);

//File System
const fs = require('fs');
const jsonData = fs.readFileSync('./data.json', 'utf8');
const parseData = JSON.parse(jsonData);

// console.log(parseData);
class FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        this.id = id,
            this.type = type;
        this.age = age;
        this.fruits = fruits || 0;
        this.totalFruits = totalFruits || 0;
        this.matureAge = matureAge;
        this.stopProducing = stopProducing;
    }
}

class AppleTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}
class PearTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}
class OtherTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}

class Tree {
    static listTrees() {
        let tempData = [];
        parseData.forEach(data => {
            const { id, type, age, fruits, totalFruits, matureAge, stopProducing } = data;
            switch (type) {
                case "Apple":
                    tempData.push(new AppleTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
                case "Pear":
                    tempData.push(new PearTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
                default:
                    tempData.push(new OtherTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
            }
        })
        return tempData;
    }
    static addTree(data) {
        let listTrees = this.listTrees();
        const { id, type, age, fruits, totalFruits, matureAge, stopProducing } = data;
        switch (type) {
            case "Apple":
                listTrees.push(new AppleTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
            case "Pear":
                listTrees.push(new PearTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
            default:
                listTrees.push(new OtherTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
        }
        this.save(listTrees);
    }
    static save(listTrees){
        fs.writeFileSync('./data.json', JSON.stringify(listTrees,null,2));
        console.log("File has been saved. Thanks.")
    }
    
}

// Tree.listTrees();

// const meats = {
//     id : 1,
//     name : "Beef",
//     isCooked : true
// }
// console.log(meats.id);
// console.log(meats.name);
// console.log(meats.isCooked);
// // Destructuring Object
// const { id,name,isCooked } = meats;
// console.log(id);
// console.log(name);
// console.log(isCooked);

const peachTree = {
    id: 3,
    type: "Peach",
    age: 1,
    fruits: 0,
    totalFruits: 0,
    matureAge: 3,
    stopProducing: 10
}

Tree.addTree(peachTree);
