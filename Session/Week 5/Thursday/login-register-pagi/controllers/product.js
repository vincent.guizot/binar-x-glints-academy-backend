const { Product } = require('../models')

class ProductController {
    static async getProduct(req, res) {
        try {
            const result = await Product.findAll({
                order: [
                    ['id', 'ASC']
                ],
            })
            res.status(200).json(result);

        }
        catch (err) {
            res.status(500).json(err);
        }
    }

    static async addProduct(req, res) {
        const { name, info, image, price, stock} = req.body;
        const UserId = req.userData.id
        try {
            const found = await Product.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another name arigato."
                })
            } else {
                const product = await Product.create({
                    name, info, image, price, stock, UserId
                })

                res.status(201).json(product)
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }
}

module.exports = ProductController;