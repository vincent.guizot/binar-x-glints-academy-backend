const { Router } = require('express');
const router = Router();
const ShipController = require('../controllers/Ship')

router.get('/', ShipController.getShip);
router.get('/add', ShipController.addFormShip)
router.post('/add', ShipController.addShip)
router.get('/edit/:id', ShipController.editFormShip)
router.post('/edit/:id', ShipController.editShip)
router.get('/info/:id', ShipController.infoShip)
router.post('/info/:id', ShipController.addCrew)
router.get('/delete/:id', ShipController.deleteShip)
router.get('/:id', ShipController.findById)

module.exports = router;