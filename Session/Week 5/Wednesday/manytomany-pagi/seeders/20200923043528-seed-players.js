'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const seedPlayers = [
     {
       name : "first_class",
       info : "Just a regular player yeah",
       image : "https://cdn.quotesgram.com/img/19/56/1102340414-spartan-warrior.jpg",
       level : 100,
       createdAt : new Date(),
       updatedAt : new Date(),
     },
     {
      name : "main_weapon",
      info : "Freshly baked from oven",
      image : "https://i.redd.it/5ttf6xbpufyy.jpg",
      level : 50,
      createdAt : new Date(),
      updatedAt : new Date(),
    }
   ]
   await queryInterface.bulkInsert('Players', seedPlayers, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Players', null, {});
  }
};
