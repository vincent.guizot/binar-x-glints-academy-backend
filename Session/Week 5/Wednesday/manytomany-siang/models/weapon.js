'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Weapon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Weapon.belongsToMany(models.Player, { through: 'models.Inventory' });
    }
  };
  Weapon.init({
    name: {
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Name must be filled thanks."
        }
      }
    },
    info:{
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Information must be filled thanks."
        }
      }
    },
    image: {
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Image must be filled thanks."
        },
        isUrl : {
          msg : "Image must be URL format!"
        }
      }
    },
    power: {
      type : DataTypes.INTEGER,
      validate : {
        notEmpty: {
          msg : "Power must be filled thanks."
        },
        isNumeric : {
          msg : "Power must be a number."
        }
      }
    },
  }, {
    hooks : {
      beforeCreate(weapon, options){
        weapon.image = "https://static2.gunfire.com/eng_pl_CM350M-Metal-Version-Shotgun-Replica-1152210910_1.jpg"
      }
    },
    sequelize,
    modelName: 'Weapon',
  });
  return Weapon;
};