const { Router } = require('express');
const router = Router();
const InventoryController = require('../controllers/Inventory')

router.get('/', InventoryController.getInventory);
router.get('/add', InventoryController.addFormInventory)
router.post('/add', InventoryController.addInventory)
// router.get('/delete/:id', InventoryController.deleteInventory)
// router.get('/edit/:id', InventoryController.editFormInventory)
// router.post('/edit/:id', InventoryController.editInventory)
// router.get('/:id', InventoryController.findById)

module.exports = router;