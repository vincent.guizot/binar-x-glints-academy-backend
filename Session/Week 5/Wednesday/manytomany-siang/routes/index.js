const { Router } = require('express');
const router = Router();
const PlayerRoutes = require('./player')
const WeaponRoutes = require('./weapon')
const InventoryRoutes = require('./inventory')

router.get('/', (req,res)=>{
    res.render('index.ejs')
});
router.use('/players', PlayerRoutes)
router.use('/weapons', WeaponRoutes)
router.use('/inventories', InventoryRoutes)

module.exports = router;