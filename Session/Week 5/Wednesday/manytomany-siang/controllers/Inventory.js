const { Inventory,Player,Weapon } = require('../models')

class InventoryController {
    static async getInventory(req, res) {
        try {
            const result = await Inventory.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include : [
                    Player,Weapon
                ]
            })
            res.status(200).json(result);

        }
        catch (err) {
            res.status(500).json(err);
        }
    }

    static addFormInventory(req, res) {
        res.render('addInventorys.ejs');
    }
    static async addInventory(req, res) {
        const { PlayerId, WeaponId } = req.body;
        try {
            const inventory = await Inventory.create({
                PlayerId, WeaponId
            })

            res.status(201).json(inventory)
            // const found = await Inventory.findOne({
            //     where: {
            //         PlayerId
            //     }
            // })
            // if (found) {
            //     res.status(409).json({
            //         msg: "Name already exist! Try another PlayerId arigato."
            //     })
            // } else {
                
            // }
        } catch (err) {
            res.status(500).json(err)
        }
    }

    // static findById(req, res) {
    //     const id = req.params.id;
    //     Inventory.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static deleteInventory(req, res) {
    //     const id = req.params.id;
    //     Inventory.destroy({
    //         where: { id }
    //     })
    //         .then(() => {
    //             // res.send("Deleted")
    //             res.redirect('/Inventorys')
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static editFormInventory(req, res) {
    //     const id = req.params.id;
    //     // console.log(id)
    //     Inventory.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             console.log(result)
    //             res.render('editInventorys.ejs', { Inventory: result });
    //         })
    //         .catch(err => {
    //             res.send(err);
    //         })
    // }

    // static editInventory(req, res) {
    //     const id = req.params.id;
    //     const { PlayerId, status, bounty } = req.body;
    //     Inventory.update({
    //         PlayerId,
    //         status,
    //         bounty,

    //     }, {
    //         where: { id }
    //     })
    //         .then(result => {
    //             if (result[0] === 1) {
    //                 res.redirect('/Inventorys')
    //             } else {
    //                 res.send('Update not done!')
    //             }
    //             // res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }
}

module.exports = InventoryController; 