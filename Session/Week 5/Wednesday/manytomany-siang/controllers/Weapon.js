const { Weapon } = require('../models')

class WeaponController {
    static async getWeapon(req, res) {
        try {
            const result = await Weapon.findAll({
                order: [
                    ['id', 'ASC']
                ],
            })
            res.status(200).json(result);

        }
        catch (err) {
            res.status(500).json(err);
        }
    }

    static addFormWeapon(req, res) {
        res.render('addWeapons.ejs');
    }
    static async addWeapon(req, res) {
        const { name, info, image, power } = req.body;
        try {
            const found = await Weapon.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another name arigato."
                })
            } else {
                const weapon = await Weapon.create({
                    name, info, image, power
                })

                res.status(201).json(weapon)
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }

    // static findById(req, res) {
    //     const id = req.params.id;
    //     Weapon.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static deleteWeapon(req, res) {
    //     const id = req.params.id;
    //     Weapon.destroy({
    //         where: { id }
    //     })
    //         .then(() => {
    //             // res.send("Deleted")
    //             res.redirect('/Weapons')
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static editFormWeapon(req, res) {
    //     const id = req.params.id;
    //     // console.log(id)
    //     Weapon.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             console.log(result)
    //             res.render('editWeapons.ejs', { Weapon: result });
    //         })
    //         .catch(err => {
    //             res.send(err);
    //         })
    // }

    // static editWeapon(req, res) {
    //     const id = req.params.id;
    //     const { name, status, bounty } = req.body;
    //     Weapon.update({
    //         name,
    //         status,
    //         bounty,

    //     }, {
    //         where: { id }
    //     })
    //         .then(result => {
    //             if (result[0] === 1) {
    //                 res.redirect('/Weapons')
    //             } else {
    //                 res.send('Update not done!')
    //             }
    //             // res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }
}

module.exports = WeaponController; 