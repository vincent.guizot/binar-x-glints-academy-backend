# feedbacks-time

## API Documentation

Link Heroku : http://heroku.app

### RESTful Endpoints

GET /teachers
Get all Teachers

Request :

- Header :
{
    "access_token" : "<your_access_token>"
}
- Body :
not needed

Response (200) :
[
     {
        "id": "<asset_id>",
        "name": "<asset_name>",
        "score": <asset_score>,
        "subject": "<asset_subject>",
        "image": "<asset_image>",
        "EmployerId": 1,
        "createdAt": "2020-10-01T03:44:50.635Z",
        "updatedAt": "2020-10-01T03:44:50.635Z",
        "Employer": {
            "id": 1,
            "name": "CUBE Entertainment",
            "createdAt": "2020-10-01T03:50:30.945Z",
            "updatedAt": "2020-10-01T03:50:30.945Z"
        }
    }
]
Response (500) : [
    "message": "<returned error message>"
]