const { Router } = require('express');
const router = Router();
const FeedbackController = require('../controllers/feedback')

const { authentication, authorization } = require('../middlewares/auth')

router.get('/',FeedbackController.getFeedback)
router.post('/teacher/:id', authentication, FeedbackController.addFeedback)
router.delete('/teacher/:id',authentication, authorization, FeedbackController.deleteFeedback)
// router.put('/:id',FeedbackController.updateFeedback)

module.exports = router;