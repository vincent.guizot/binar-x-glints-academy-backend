const { Router } = require('express');
const router = Router();
const EmployerController = require('../controllers/employer')

router.get('/',EmployerController.getEmployer)
router.post('/',EmployerController.addEmployer)
router.delete('/:id',EmployerController.deleteEmployer)
// router.put('/:id',EmployerController.updateEmployer)

module.exports = router;