const { Router } = require('express');
const router = Router();
const TeacherController = require('../controllers/teacher')

router.get('/',TeacherController.getTeacher)
router.post('/',TeacherController.addTeacher)
router.delete('/:id',TeacherController.deleteTeacher)
// router.put('/:id',TeacherController.updateTeacher)

module.exports = router;