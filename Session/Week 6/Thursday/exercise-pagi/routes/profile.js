const { Router } = require('express');
const router = Router();
const ProfileController = require('../controllers/profile')

router.get('/',ProfileController.getProfile)
router.post('/',ProfileController.addProfile)
router.delete('/:id',ProfileController.deleteProfile)
// router.put('/:id',ProfileController.updateProfile)

module.exports = router;