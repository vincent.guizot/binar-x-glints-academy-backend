const { Feedback, Student, Teacher } = require('../models')

class FeedbackController {
    static async getFeedback(req, res, next) {
        try {
            const result = await Feedback.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include : [
                    Student, Teacher
                ]
            })
            res.status(200).json(result);
        }
        catch (err) {
            next(err)
        }
    }

    static async addFeedback(req, res, next) {
        const { info, score } = req.body;
        const StudentId = req.userData.id;
        const TeacherId = req.params.id ;
        try {
            const feedback = await Feedback.create({
                info, score , TeacherId, StudentId
            })

            res.status(201).json(feedback)
        } catch (err) {
            next(err);
        }
    }

    static async deleteFeedback(req, res, next) {
        const id = req.params.id;

        try {
            const result = Feedback.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                result,
                msg: "Feedback deleted"
            })
        } catch (err) {
            next(err)
        }
    }
}

module.exports = FeedbackController;