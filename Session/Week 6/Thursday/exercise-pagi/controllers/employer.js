const { Employer } = require('../models')

class EmployerController {
    static async getEmployer(req, res, next) {
        try {
            const result = await Employer.findAll({
                order: [
                    ['id', 'ASC']
                ],
                
            })
            res.status(200).json(result);

        }
        catch (err) {
            next(err)
        }
    }

    static async addEmployer(req, res, next) {
        const { name } = req.body;
        //Menerima dari Middlewares authentication
        // const UserId = req.userData.id
        try {
            const employer = await Employer.create({
                name 
            })

            res.status(201).json(employer)
        } catch (err) {
            next(err);
        }
    }

    static async deleteEmployer(req, res, next) {
        const id = req.params.id;

        try {
            const result = Employer.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                result,
                msg: "Employer deleted"
            })
        } catch (err) {
            next(err)
        }
    }
}

module.exports = EmployerController;