const { Student, Profile } = require('../models')
const {decryptPwd} = require('../helpers/bcrypt')
const {tokenGenerator} = require('../helpers/jwt')

class StudentController {
    static async list(req, res) {
        try {
            const students = await Student.findAll();

            res.status(200).json(students)
        } catch (err) {
            res.status(500).json(err)
        }
    }
    static async login(req, res) {
        const { username, password } = req.body;
        
        try {
            const student = await Student.findOne({
                where : {
                    username
                }
            })
            if(student){
                if(decryptPwd(password, student.password)){
                    
                    const access_token = tokenGenerator(student)
                    res.status(200).json({access_token})
                }else{
                    res.status(404).json({
                        msg : "Pwd is not the same."
                    })
                }

            }else{
                res.status(404).json({
                    msg : "Student is not found thanks."
                })
            }

        }catch (err) {
            res.status(500).json(err)
        }
    }
    
    static async register(req, res) {
        const { username, password } = req.body;
        try {
            const student = await Student.create({
                username, password 
            })
            
            const profile = await Profile.create({
                image : "Null",
                gender : "Null",
                address : "Null",
                StudentId : student.id
            })
            
            res.status(201).json({
                student, profile
            });
        }catch (err) {
            res.status(500).json(err)
        }
    }
}

module.exports = StudentController;