'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Feedback.belongsTo(models.Student);
      Feedback.belongsTo(models.Teacher);
    }
  };
  Feedback.init({
    info: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Info must be filled thanks."
        }
      }
    }, 
    score: { 
      type : DataTypes.INTEGER,
      validate : {
        notEmpty : {
          msg : "Score must be filled thanks."
        },
        isNumeric : {
          msg : "Score must be a number."
        }
      }
    },
    TeacherId: DataTypes.INTEGER,
    StudentId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Feedback',
  });
  return Feedback;
};