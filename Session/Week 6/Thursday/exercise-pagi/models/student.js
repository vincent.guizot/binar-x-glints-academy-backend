'use strict';
const {encryptPwd} = require('../helpers/bcrypt')

const { 
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Student.hasOne(models.Profile);
      Student.belongsToMany(models.Teacher, {through: 'models.Feedback'});
    }
  };
  Student.init({
    username: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Username must be filled thanks."
        }
      }
    },
    password: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Pwd must be filled thanks."
        }
      }
    },
  }, {
    hooks : {
      beforeCreate(student){
        student.password = encryptPwd(student.password)
      }
    },
    sequelize,
    modelName: 'Student',
  });
  return Student;
};