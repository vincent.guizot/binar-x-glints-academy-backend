const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

const mongoose = require('mongoose')
const router = require('./routes')

//Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

//Routes
app.use(router);

mongoose.connect('mongodb://localhost/learn-mongo', { useNewUrlParser: true, useUnifiedTopology : true })
.then(()=>{
    console.log("Mongo DB has been connected!")
})
.catch(err=>{
    console.log("Message : ", err)
})

app.listen(PORT, () => {
    console.log(`Server is running at port : ${PORT}`);
})
