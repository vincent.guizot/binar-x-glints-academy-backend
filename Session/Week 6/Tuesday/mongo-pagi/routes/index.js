const router = require('express').Router();

const IndexRoute =  (req,res) => {
    res.status(200).json({
        message: 'Home'
    })
}

// Users
const {UserController} = require('../controllers/')
router.get('/users', UserController.viewUsers)
router.post('/users/register', UserController.register)
router.post('/users/login', UserController.login)


router.get('/', IndexRoute)

module.exports = router;