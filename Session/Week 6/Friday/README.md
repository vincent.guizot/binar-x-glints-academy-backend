# ecommerce-cms
Creating e-commerce-cms app
App is an application to manage your assets. This app has :RESTful endpoint for e-commerce-cms app CRUD operation

Link Web 
# https://cms-ecommerce-5ff7a.web.app

RESTful endpoints
GET /product

Get all product
Request Header{
  "access_token": "<your access_token>"
}
Request Body
not needed

Response (200)[
  {
    "id": 1,
    "name": "<asset name>",
    "image_url": "<asset image_url>",
    "price": "<asset price>",
    "stock": "<asset stock>",
    "UserId": "<asset UserId>"
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]
Response (400 - Bad Request){
  "message": "<returned error message>"
}

POST /product

Create new product
Request Header{
  "access_token": "<your access_token>"
}
Request Body{
    "name": "<asset name>",
    "image_url": "<description image_url>",
    "price": "<asset price>",
    "stock": "<asset stock>"
}
Response (201 - Created){
    "id": <given id by system>,
    "name": "<posted name>",
    "image_url": "<posted image_url>",
    "price": "<posted price>",
    "stock": "<posted stock>"
    "UserId": "<posted UserId>",
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
}
Response (400 - Bad Request){
  "message": "<returned error message>"
}

PUT /product/:id
Update product by id
Request Header{
  "access_token": "<your access_token>"
}
Request Body to update{
    "name": "<asset name>",
    "image_url": "<description image_url>",
    "price": "<asset price>",
    "stock": "<asset stock>"
}
Response (201 - Created){
    "id": <given id by system>,
    "name": "<posted name>",
    "image_url": "<posted image_url>",
    "price": "<posted price>",
    "stock": "<posted stock>"
    "UserId": "<posted UserId>",
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
}
Response (400 - Bad Request){
  "message": "<returned error message>"
}

DELETE /product/:id
delete product by id
Request Header{
  "access_token": "<your access_token>"
}
Request Body to delete
not needed

Response (201 - deleted) all assets except the one that has been deleted

Response (400 - Bad Request){
  "message": "<returned error message>"
}

RESTful endpoint for User's CRUD operation
JSON formatted response
POST /user/auth/register

POST User to register
Request Header
not needed

Request Body{
    "name": "<asset username>",
    "email": "<asset email>",
    "password": "<asset password>"
}
Response (201)[
  {
    "id": 1,
    "name": "<asset username>",
    "email": "<asset email>",
    "password": "<asset password>",
    "role": "<asset role>",
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]
Response (400 - Bad Request){
  "message": "<returned error message>"
}
POST /user/auth/login

POST User to login
Request Header
not needed

Request Body{
    "email": "<asset email>",
    "password": "<asset password>",
}
Response (200)[
  {
    "access_token": "<your access_token>"
  }
]
Response (400 - Bad Request){
  "message": "<returned error message>"
}
