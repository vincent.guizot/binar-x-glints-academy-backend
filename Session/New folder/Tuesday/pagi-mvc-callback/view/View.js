class View {
    static list(data){
        // console.log(data);
        data.forEach(el => {
            console.log(`${el.id}. ${el.name}, category : ${el.category}.`);
        });
    }
    static message(data){
        console.log(data);
    }
    static error(err){
        console.log("Hasil Error", err);
    }
}

module.exports = View;