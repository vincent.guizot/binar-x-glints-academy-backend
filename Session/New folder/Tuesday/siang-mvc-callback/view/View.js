class View {
    static list(data){
        data.forEach(el => {
            console.log(`${el.id}. ${el.name}, category : ${el.category}`);
        })
    }
    static message(msg){
        console.log(msg);
    }
    static error(err){
        console.log(err);
    }
}

module.exports = View;