const { Router } = require('express');
const router = Router();
const StudentController = require('../controllers/Student')

router.get('/', StudentController.getStudent)
router.get('/add', StudentController.addFormStudent)
router.post('/add', StudentController.addStudent)
router.get('/delete/:id', StudentController.deleteStudent)
router.get('/edit/:id', StudentController.updateStudent)
router.get('/:id', StudentController.findById)

module.exports = router;