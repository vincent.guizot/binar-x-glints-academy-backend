const { Router } = require('express');
const router = Router();
const StudentRoutes = require('./student')

router.get('/', (req,res)=>{
    res.render('index.ejs')
});

router.use('/students', StudentRoutes)

// router.get('/lecturers', (req, res) => {
//     res.send('Ini page lecturers')
// })

module.exports = router;