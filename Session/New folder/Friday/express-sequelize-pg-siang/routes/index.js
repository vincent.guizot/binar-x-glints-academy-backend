const { Router } = require('express');
const router = Router();


const StudentController = require('../controllers/student')

router.get('/', StudentController.list)

router.get('/students', (req, res) => {
    res.send('Ini page students')
})

router.get('/lecturers', (req, res) => {
    res.send('Ini page lecturers')
})

module.exports = router;