const Product = require('../model/Product');
const View = require('../view/View');

class ProductController{
    static list(){
        const list = Product.list();
        View.list(list);
    }
    static add(params){
        const result = Product.add(params);
        View.message(result);
    }
    static delete(params){
        const result = Product.delete(params);
        View.message(result);
    }
    static update(params){
        const result = Product.update(params);
        View.message(result);
    }
    static message(){
        View.message("Masukkan perintah yang benar trims.");
    }
}

module.exports = ProductController;