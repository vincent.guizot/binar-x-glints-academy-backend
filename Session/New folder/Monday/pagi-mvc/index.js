const command = process.argv[2];
const params = process.argv.slice(3);
const ProductController = require('./controller/ProductController')

switch (command) {
    case 'list':
        ProductController.list();
        break;
    case 'add':
        ProductController.add(params);
        break;
    case 'delete':
        ProductController.delete(params);
        break;
    case 'update':
        ProductController.update(params);
        break;
    default:
        ProductController.message();
        break;
}

// Flow MVC
// 1. User / pengguna input terminal ke index.js -> node index.js
// 2. Logic Flow : Controller -> Model -> Controller -> View