const { Router } = require('express');
const router = Router();
const ProductRouter = require('./product');
 
router.get('/', (req,res) => {
    // res.send("HOME PAGE");
    res.render('index.ejs');
})

router.use('/product', ProductRouter);

module.exports = router;