const express =  require('express');
const app = express();
const port = 3000;


app.get('/',  (req,res) =>{
    res.json({
        id: 1,
        task: 'Hunting'
    });
})

app.listen(port , ()=>{
    console.log("Running at port", port)
})