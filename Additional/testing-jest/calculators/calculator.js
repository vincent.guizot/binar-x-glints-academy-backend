const sum = (a, b) => {
    return a + b;
}

const sub = (a, b) => {
    return a - b
}

const mul = (a, b) => {
    return a * b
}

const div = (a, b) => {
    return a / b
}

const pow = (a, b) => {
    let total = 1;
    for (let i = 0; i < b; i++) {
        total = total * a
    }
    return total
}
module.exports = {
    sum, sub, mul, div, pow
};