const { hello, combineWords } = require('../strings/string')

describe('String Function', () => {

    describe('Hello function', () => {

        test('Hello function return "Hello World"', () => {
            const helloBaru = hello("Hello World");

            expect(helloBaru).toBe("Hello World");
        })

        test('Hello function doesn"t return "Hello World"', () => {
            const helloBaru1 = hello("hello world");

            expect(helloBaru1 !== "Hello World").toBe(true);
        })

    })

    describe('CombineWords function', ()=>{
        test("CombineWords should return right result", ()=> {
            const combinedResults = combineWords("Arman","Loves","503")
            
            expect(combinedResults).toBe("Arman Loves 503")
        })

    })
})
