const { sum, sub, mul, div, pow } = require('../calculators/calculator')

describe('Calculator Function', () => {
    describe('Testing function sum', () => {
        test('Hasil sum true', () => {
            const jumlah = sum(1, 2);

            expect(jumlah).toBe(3);
        })
        test('Hasil sum salah', () => {
            const jumlah = sum(1, 2);

            expect(jumlah !== 3).toBe(false);
        })
        test('Hasil sum kurang dari 0', () =>{
            const jumlah = sum (-1,-2);

            expect(jumlah < 0).toBe(true);
        })

    })

    describe('Testing function sub', () => {
        test('Hasil sub true', () => {
            const kurang = sub(5, 7);

            expect(kurang).toBe(-2);
        })
        test('Hasil sub false', () => {
            const kurang = sub(5, 7);

            expect(kurang !== -2).toBe(false);
        })
    })

    describe('Testing function mul', () => {
        test('Testing function mul', () => {
            const kali = mul(3, 5)

            expect(kali).toBe(15)
        })
    })
    describe('Testing function div', () => {

        test('Testing function div', () => {
            const bagi = div(30, 3)

            expect(bagi).toBe(10)
        })
    })
    describe('Testing function power', () => {
        test('Testing function power', () => {
            const pangkat = pow(2, 12)

            expect(pangkat).toBe(4096)
        })
    })
})