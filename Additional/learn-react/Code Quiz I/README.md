# "Code Quiz I"

> KERJAKAN

## TTasks

### Task A -- Cari Faktor Prima

```javascript
    const faktor = (arr) => {
        // code here
    }

    // Test
    faktor(70) // [2,5,7]
    faktor(119) // [7,17]
```

### Task B --> Ganjil, Genap dan Tiga

```javascript
    const groupAngka = (arr) => {
        // code here
    }

    // T
    grouAngka([2, 4, 6]); // [ [2, 4], [], [6] ]
    grouAngka([1, 2, 3, 4, 5, 6, 7, 8, 9]); // [ [ 2, 4, 8 ], [ 1, 5, 7 ], [ 3, 6, 9 ] ]
    grouAngka([100, 151, 122, 99, 111]); // [ [ 100, 122 ], [ 151 ], [ 99, 111 ] ]
    grouAngka([]); // [ [], [], [] ]

```

### Task C --> Chairs

```javascript

    //DRIVER CODE

    console.log(sittingArrangement(['A', 'B', 'C'], 0 )); //Invalid number
    console.log(sittingArrangement([ 'Juli', 'Nisa', 'Desi', 'Ulfa', 'Puji' ], 2)); //[ [ 'Juli', 'Nisa' ], [ 'Desi', 'Ulfa' ], [ 'Puji', 'Kursi Kosong' ] ]
    console.log(sittingArrangement([ 'Yosia', 'Asrawi', 'Andru' ], 3)); //[ [ 'Yosia', 'Asrawi', 'Andru' ] ]
    console.log(sittingArrangement([ 'Lukman', 'Adam', 'Dimas', 'Hansin', 'Orion' ], 4));
    // [
    //   [ 'Lukman', 'Adam', 'Dimas', 'Hansin' ],
    //   [ 'Orion', 'Kursi Kosong', 'Kursi Kosong', 'Kursi Kosong' ]
    // ]
```

## How To Submit

1. Buat branch baru
